
// Item 2 - Fruits per country
db.fruits.aggregate([
    {
        $unwind:"$origin"
    }, 
    {
        $group:{_id:"$origin", fruits:{$sum:1}}
    }
])



// Item 2 - Fruits On Sale
db.fruits.aggregate([
    {
        $match:{onSale:true}
    }, 
    {
        $group:{_id: null, fruitsOnSale:{$sum:1}}
    },
    {
        $project:{_id:0}
    }
])


// Item 3 - Enough stock
db.fruits.aggregate([
    {
        $match:{stock:{$gte:20}}
    },
    {
        $count: "enoughStock"
    }
]) 

//Item 4 - Average
db.fruits.aggregate([
    {
        $match:{onSale:true}
    },
    {
        $group:{_id:"$supplier_id", avg_price: { $avg:"$price"}}
    }
])


//Item 5 - Max
db.fruits.aggregate([
    {
        $match:{onSale:true}
    },
    {
        $group:{_id:"$supplier_id", max_price: { $max:"$price"}}
    }
])

//Item 6 - Min
db.fruits.aggregate([
    {
        $match:{onSale:true}
    },
    {
        $group:{_id:"$supplier_id", min_price: { $min:"$price"}}
    }
])